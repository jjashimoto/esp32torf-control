#include <RCSwitch.h>
#include <EEPROM.h>


#define RF_MAX_CODES 2
#define EEPROM_SIZE  ((RF_MAX_CODES * 3) + 1)

RCSwitch mySwitch = RCSwitch();

struct code {
  byte  byte4;
  byte  byte3;
  byte  byte2;
  byte  byte1;
};


union rfDatos{
  struct code RXcodes;
  int received;
} rxdata;
int rfrxPin = 4;
int debounce,button_delay,lear_timeout;
bool antidebounce=0;
bool rf_learn = 0;
bool vrf_button;


bool saved_rf();
void rf_button();


void setup() {
  pinMode(2,OUTPUT); //bluild in LED
  pinMode(23,PULLUP); //RF learn/clear button
  Serial.begin(9600);
  Serial.println("booting");
  Serial.println(EEPROM_SIZE);
  mySwitch.enableReceive(digitalPinToInterrupt(rfrxPin));
  
  
  EEPROM.begin(EEPROM_SIZE);
  EEPROM.write(0,0);
  
  
  
  ///codigos en EEPROM entrada manual
  EEPROM.write(1,0); //0x95);
  EEPROM.write(2,0); //0x37);
  EEPROM.write(3,0); //0x34);
// 
  EEPROM.write(4,0); //0xA5);
  EEPROM.write(5,0); //0x37);
  EEPROM.write(6,0); //0x34);

}

void loop() {
  if(mySwitch.available() && antidebounce == 0){
    rxdata.received = mySwitch.getReceivedValue();
    mySwitch.resetAvailable();
      Serial.print(" Received ");
      Serial.println(rxdata.received);

      antidebounce = saved_rf();
  }
  else{
    mySwitch.resetAvailable();
  } 
  if ((millis()-debounce) > 700){
    antidebounce = 0;
    digitalWrite(2,LOW);
  }
   rf_button(); //checa boton aprender
}

void loop2(){
  
}

void rf_button(){
    if( !digitalRead(23) ){
      //Boton presionado
      if(vrf_button == 0){
        vrf_button = 1;
        button_delay = millis();
      }
    }
    else if(digitalRead(23) && millis()- button_delay > 1000 && millis()- button_delay < 10000 && vrf_button == 1 && rf_learn == 0){
      //activa el modo aprender
      Serial.println("Aprendiendo codigo");
      vrf_button=0;
      rf_learn=1;
      lear_timeout = millis();
    }
    else if(digitalRead(23) && millis()- button_delay > 10000 && vrf_button == 1 && rf_learn == 0){
      //borra todos los controles
      Serial.println("Borrando Controlles");
      vrf_button=0;
      rf_learn=0;
    }
    if(rf_learn==1 && millis()-lear_timeout>10000){
      Serial.println("RF Learn timeout");
      rf_learn=0;
    }


}

bool saved_rf(){
    int index = EEPROM.read(0);
    int mcode;
    for(int i = 0; i < index ; i++){
       mcode=3 * i;
      if(EEPROM.read(mcode + 1)== rxdata.RXcodes.byte4)
        if(EEPROM.read(mcode + 2)== rxdata.RXcodes.byte3)
          if(EEPROM.read(mcode + 3)== rxdata.RXcodes.byte2){
           if(rf_learn == 0){ 
              digitalWrite(2,HIGH);        
              debounce = millis();
              return 1; //activa el debounce
            }
           else
           {
              rf_learn = 0; // encontro el codigo grabado
              debounce = millis();
              return 1;  //activa el debounce 
           }
            
        }
    }
   if(rf_learn == 1 && index < (RF_MAX_CODES) ){
     mcode = 3 * index;
     EEPROM.write(mcode + 1, rxdata.RXcodes.byte4); //code 1 byte
     EEPROM.write(mcode + 2, rxdata.RXcodes.byte3); //code 2 byte
     EEPROM.write(mcode + 3, rxdata.RXcodes.byte2); //code 3 byte
     EEPROM.write(0,index+1); //update index
     rf_learn=0;
     debounce = millis();
     return 1; // activa el debounce
   }
   else if(rf_learn == 1 && index == (RF_MAX_CODES)){
     rf_learn=0;
     Serial.println("Memoria llena");
   }
    
  return 0;
}